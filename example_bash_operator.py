# -*- coding: utf-8 -*-

from datetime import timedelta
from helper.foo import test
from helper.foo import test_conn_by_airflow
from helper.foo import ReturningMySqlOperator, get_records

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

args = {
    'owner': 'Airflow',
    'start_date': airflow.utils.dates.days_ago(0),
}

dag = DAG(
    dag_id='test_dag',
    default_args=args,
    schedule_interval=None,
)


op_test = PythonOperator(
    task_id='py_op',
    python_callable=test,
    dag=dag
)
op_test_conn = PythonOperator(
    task_id='mariadb_conn_test',
    python_callable=test_conn_by_airflow,
    dag=dag
)

t1 = ReturningMySqlOperator(
    task_id='basic_mysql',
    mysql_conn_id='airflow_db',
    sql="select * from xcom",
    dag=dag)

t2 = PythonOperator(
    task_id='records',
    provide_context=True,
    python_callable=get_records,
    dag=dag)

t1 >> t2