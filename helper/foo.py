import pymysql.cursors
from airflow.hooks.mysql_hook import MySqlHook
from airflow.operators.mysql_operator import MySqlOperator

def test():
    print('Print output')
    return 'return output'

def test_connect(host='192.168.1.113'):
    return pymysql.connect(
        host=host,
        port=30769,
        user='admin',
        password='admin',
        db='default',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor)

def test_conn_by_airflow():
    conn_id='datapipeline - mariadb'
    mysql = MySqlHook(mysql_conn_id=conn_id)
    return mysql.get_conn()

class ReturningMySqlOperator(MySqlOperator):
    def execute(self, context):
        self.log.info('Executing: %s', self.sql)
        hook = MySqlHook(mysql_conn_id=self.mysql_conn_id,
                         schema=self.database)
        return hook.get_records(
            self.sql,
            parameters=self.parameters)

def get_records(**kwargs):
    ti = kwargs['ti']
    xcom = ti.xcom_pull(task_ids='basic_mysql')
    string_to_print = 'Value in xcom is: {}'.format(xcom)
    # Get data in your logs
    logging.info(string_to_print)